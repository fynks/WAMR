<div align="center">
  <img src="assets/images/iconreader.png" alt = "wareader" width ="128px">
  <p>Now with POV messages</p>
  <h1>WhatsApp Reader</h1>
  <p>Interpreat the exported chat from WhatsApp to something readable.</p><br>
</div>
  
# How to export a chat?
You can read the [FAQ in the WA page for export a chat](https://faq.whatsapp.com/196737011380816/?helpref=uf_share).

# How to use WA Reader
Only need the `.txt` of the chat and upload in the entry of the site. after that, you can see all the chat like WhatsApp style.

# Issues 
- Laggy when you treat to show at least 40k of messages.

# Preview
You can see [WhatsApp Reader](https://davidquintr.github.io/whatsapp-reader.github.io/) just here.

<div align="center">
    <br>
    <img src="assets/images/davidquint.png" width="96">
    <p>Made by DavidQuint</p>
</div>

# _Changelogs_

## **First release v1.0.0**

_Includes all the improvements for the basic messages._

- Multiline messages
- POV messages
- Different colors for all users in the conversation
- Showing all the warnings
- Datetime showing in the top of view.
